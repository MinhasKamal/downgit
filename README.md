# DownGit
#### Create GitHub Directory Download Link

### How to Use?

![DownGit Use Demo](https://cloud.githubusercontent.com/assets/5456665/17612456/ccf55fea-6073-11e6-9d58-7542fd9351f0.gif)

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-127x51.png" /></a><br/>DownGit is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
